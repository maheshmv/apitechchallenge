using System;
using Xunit;
using ApiTechChallenge.Model;
using AutoFixture;
using AutoFixture.AutoMoq;
using Moq;
using ApiTechChallenge.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Apitechchallenge.Tests
{
    public class APIControllerTests
    {
        IHttpClientHandler _client;
        AnswersController sut;

        void Setup()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            var clientMock = fixture.Freeze<Mock<IHttpClientHandler>>();

            var prodList = fixture.Create<IEnumerable<Product>>();
            clientMock.Setup(x => x.GetAsync<Product>(It.IsAny<string>())).Returns(Task.FromResult(prodList));

            var prodPurchasesList = fixture.Create<IEnumerable<ProductPurchases>>();
            clientMock.Setup(x => x.GetAsync<ProductPurchases>(It.IsAny<string>())).Returns(Task.FromResult(prodPurchasesList));
            fixture.Customize<BindingInfo>(c => c.OmitAutoProperties());

            sut = fixture.Create<AnswersController>();


        }
        [Fact]
        public async void TestSortedList()
        {
            Setup();
             var response = await sut.GetProductsSorted("low");

            var okResult = response.Result as OkObjectResult;
            
             Assert.IsType<ActionResult<List<Product>>>(response);

            Assert.IsType<IEnumerable<Product>>(okResult.Value);
            
        }
    }
}
