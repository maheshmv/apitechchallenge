﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ApiTechChallenge.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace ApiTechChallenge.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AnswersController : ControllerBase
    {
        private ApiValues apiValues;
        IHttpClientHandler httpclient;

        public AnswersController(IOptions<ApiValues> config,  IHttpClientHandler _httpClient )
        {
            apiValues = config.Value;
            httpclient = _httpClient;
        }

        [HttpGet]
        [Route("")]
        [Route("user")]
        public IActionResult Get()
        {
            if (Request.Path == "/answers")
                return Redirect("/answers/user");
            else
                return Ok("Yes");
        }

        private async Task<IEnumerable<TopProducts>> GetShopperHistory()
        {
            
                var response = await httpclient.GetAsync<ProductPurchases>(apiValues.ShopperHistoryUrl);
                if (response != null)
                {
                    IEnumerable<ProductPurchases> shopperHistory = response;
                    var returnObject = from c in shopperHistory
                                       from p in c.Products
                                       group c by p.Name into grp
                                       select new TopProducts { ProductName = grp.Key, Count = grp.Count() };
                    return returnObject;
                }
                else
                {
                    return null;
                }
            
        }
        private string GetURI(string url)
        {
            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query[apiValues.Key] = apiValues.Token;
            uriBuilder.Query = query.ToString();
            return uriBuilder.ToString();
        }

        [Route("sort")]
        public async Task<ActionResult<List<Product>>> GetProductsSorted(string sortOption)
        {

            if (sortOption == null)
            {
                return BadRequest("No products found.");
            }

            IEnumerable<Product> productList = await httpclient.GetAsync<Product>(apiValues.ProductUrl);// client.GetAsync(GetURI(apiValues.ProductUrl));
            switch (sortOption.ToLower())
            {

                //CallMethod(sortOption,sortOptionClass())

                case "low":
                    return Ok(productList.OrderBy(p => p.Price));
                case "high":
                    return Ok(productList.OrderByDescending(p => p.Price));
                case "ascending":
                    return Ok(productList.OrderBy(p => p.Name));
                case "descending":
                    return Ok(productList.OrderByDescending(p => p.Name));
                case "recommended":
                    IList<TopProducts> recommendedList = (await GetShopperHistory()).ToList();
                    var sortedList = from rc in recommendedList
                                        join pl in productList on rc.ProductName equals pl.Name into ps
                                        from p in ps.DefaultIfEmpty()
                                        select p;
                    var ret = sortedList.Union(productList);
                    return Ok(ret);
                default:
                    return BadRequest();
            }
           
        }

        [Route("trolleyTotal")]
        public async Task<ActionResult> PostTrolleyItems([FromBody] Trolley trolley)
        {
            if (trolley == null)
            {
                return BadRequest("No Trolley found.");
            }
            
            var response = await httpclient.PostAsync(trolley);
            //var response = await client.PostAsync(GetURI(apiValues.TrolleyUrl), stringContent);
            
            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync();
                return Ok(responseString);
            }
            else
            {
                return BadRequest("Please send the correct trolley object");
            }
        }

    }
}
