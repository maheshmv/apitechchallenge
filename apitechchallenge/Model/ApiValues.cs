﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTechChallenge.Model
{
    public class ApiValues
    {
        public string Key { get; set; }
        public string Token { get; set; }
        public string ProductUrl { get; set; }
        public string ShopperHistoryUrl { get; set; }
        public string TrolleyUrl { get; set; }
    }
}
