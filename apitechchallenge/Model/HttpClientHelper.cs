﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ApiTechChallenge.Model
{
    public class HttpClientHelper : IHttpClientHandler
    {
        public HttpClient httpClient { get ; set ; }

        private ApiValues apiValues;
        public HttpClientHelper(IOptions<ApiValues> config)
        {
            apiValues = config.Value;
        }

        public async Task<IEnumerable<T>> GetAsync<T>(string requestUri)
        {
            IActionResult objResult = new OkResult();

            using (var client = this.GetHttpClient())
            {
                using (var response = await client.GetAsync(GetURI(requestUri)))
                {
                    
                    string responseString = await response.Content.ReadAsStringAsync();
                    IEnumerable<T> ret = JsonConvert.DeserializeObject<IEnumerable<T>>(responseString);
                    return ret;
                }
            }
        }

        public async Task<HttpResponseMessage> PostAsync(Trolley trolley)
        {
            IActionResult objResult = new OkResult();
            SanitizeTrolley(trolley);
            StringContent stringContent = new StringContent(JsonConvert.SerializeObject(trolley), UnicodeEncoding.UTF8, "application/json");

            using (var client = this.GetHttpClient())
            {
                using (var response = await client.PostAsync(GetURI(apiValues.TrolleyUrl), stringContent)) 
                {
                    return response;
                }
            }
        }
        private HttpClient GetHttpClient()
        {
            if (httpClient == null) 
            {
                var _httpClient = new HttpClient();
                _httpClient.DefaultRequestHeaders.Accept.Clear();
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                return _httpClient;
            }
            return httpClient;
        }

        private string GetURI(string url)
        {
            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query[apiValues.Key] = apiValues.Token;
            uriBuilder.Query = query.ToString();
            return uriBuilder.ToString();
        }


        private void SanitizeTrolley(Trolley trolley)
        {
            if (trolley.Products == null)
            {
                trolley.Products = new List<TrolleyProduct>();
            }
            if (trolley.Specials == null)
            {
                trolley.Specials = new List<TrolleySpecial>();
            }
            if (trolley.Quantities == null)
            {
                trolley.Quantities = new List<TrolleyProductQuantity>();
            }
        }
    }
}
