﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace ApiTechChallenge.Model
{
    public interface IHttpClientHandler
    {
        public HttpClient httpClient { get; set; }

        Task<IEnumerable<T> > GetAsync<T>(string requestUri);
        Task<HttpResponseMessage> PostAsync(Trolley trolley);
    }
}
