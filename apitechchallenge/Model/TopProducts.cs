﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTechChallenge.Model
{
    public class TopProducts
    {
        public string ProductName { get; set; }
        public int Count { get; set; }
    }
}
