﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTechChallenge.Model
{
    public class TrolleyProduct
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
