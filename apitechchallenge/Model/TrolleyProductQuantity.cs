﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTechChallenge.Model
{
    public class TrolleyProductQuantity
    {
        public int Quantity { get; set; }
        public string Name { get; set; }
    }
}
