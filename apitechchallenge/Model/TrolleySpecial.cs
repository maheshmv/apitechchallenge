﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTechChallenge.Model
{
    public class TrolleySpecial
    {
        public decimal Total { get; set; }
        public IEnumerable<TrolleyProductQuantity> Quantities { get; set; }
    }
}
