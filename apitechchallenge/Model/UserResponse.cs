﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTechChallenge.Model
{
    public class UserResponse
    {
        public string Name { get; set; }
        public string Token { get; set; }
    }
}
